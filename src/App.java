import module.User;

public class App {
    public static void main(String[] args) throws Exception {
        User user1 = new User();

        User user2 = new User("dell123","khanhtm",true);
        System.out.println("User 1: " + user1.toString());
        System.out.println("User 2: " + user2.toString());
    }
}
