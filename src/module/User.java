package module;

public class User {
  private String Password;
  private String Username;
  private boolean Enabled;

  public String getPassword() {
    return Password;
  }
  public void setPassword(String password) {
    Password = password;
  }
  public String getUsername() {
    return Username;
  }
  public void setUsername(String username) {
    Username = username;
  }
  public boolean isEnabled() {
    return Enabled;
  }
  public void setEnabled(boolean enabled) {
    Enabled = enabled;
  }

  public User() {
  }

  public User(String password, String username, boolean enabled) {
    Password = password;
    Username = username;
    Enabled = enabled;
  }

  public String toString(){
    return "User[username=" + this.Username + ", password=" + this.Password + ", enable=" + this.Enabled + "]";
  }
}
